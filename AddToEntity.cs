// -----------------------------------------------------------------------------------
// GetSpawnPointByLevel
// -----------------------------------------------------------------------------------
public Vector3 GetSpawnPointByLevel(int spawnLevelMin = -1, int spawnLevelMax = -1) {
	
	// set min/max level both to 0 to have a spawnPoint that is only used when starting a new game
	
	if (spawnLevelMin == -1) { spawnLevelMin = level; } //set to player level if no minimum specified
	if (spawnLevelMax == -1) { spawnLevelMax = level; } //set to player level if no maximum specified
	
	var spawnPointsAll 							= FindObjectsOfType<SpawnPoint>();
	List<SpawnPoint> spawnPointsSelected 	= new List<SpawnPoint>();

	foreach (SpawnPoint mySpawnPoint in spawnPointsAll) {
		// only choose spawnPoints within the level requirements
		if (mySpawnPoint.minLevel <= spawnLevelMin && mySpawnPoint.maxLevel <= spawnLevelMax) {
			spawnPointsSelected.Add(mySpawnPoint);
		}
	} 
	
	if (spawnPointsSelected != null && spawnPointsSelected.Count > 0) {
		// return one random spawnPoint from the list
		return spawnPointsSelected[UnityEngine.Random.Range(0, spawnPointsSelected.Count)].transform.position;
	} else {
		//return a empty vector if no spawnpoint was found - this should never happen
		return new Vector3(0, 0, 0);
	}
} 