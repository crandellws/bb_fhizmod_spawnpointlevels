FhizMod - Fhizban's uMMORPG Modifications & Add-Ons
------------------------------------------------------------------------------------------

All modifications & add-ons require the uMMORPG Asset and assume that you use either a fresh
project or a correctly updated project. Modifications & Add-ons relate to the stated version
of uMMORPG. If you have any further questions, contact me on the unity forums.

Modifications require you to change the core script codes. Add-Ons on the other hand will
make use of the upcoming Add-On system.

* uMMORPG on the asset store: 	https://www.assetstore.unity3d.com/en/#!/content/51212
* uMMORPG official discussion: 	https://forum.unity3d.com/threads/ummorpg-official-thread.376636/
* uMMORPG official documentation:	https://noobtuts.com/unity/MMORPG

Check out all my uMMORPG Modfications & Add-Ons:	http://ummorpg.critical-hit.biz

It is recommended to backup your projects before applying any modifications to it!

SpawnPointLevels for uMMORPG 1.68
==========================================================================================

This simple script allows you to define a level range for spawn points. Players can only
spawn and respawn to spawn points within the level range. You can specify a game starting
spawn point with min level = 0 and max level = 0. A starter spawn point will only be used
when a brand new player is spawned. Make sure to have spawn points to cover all levels, or
your players cannot respawn. If multiple spawn points are available, one is chosen
randomly.

1. Add project file SpawnPoint.cs to your scripts folder.

2. In your project hierarchy, choose existing spawn point and click "add component".
2.a Choose the Spawn "Point Script" component.
2.b Set the Min Level and Max Level of your Spawn Point.

3. Open your Entity.cs script and add everything in the project file AddToEntity.cs to it.
	You could add it at the very end of the Entity.cs file, but before the final }
	
	Be sure that you do NOT add AddToEntity.cs to your project scripts, its not required.
	
	Just add the code inside that file to your Entity.cs
	
4. Open your Player.cs script and locate this line:

	agent.Warp(mySpawnPoint.transform.position); // recommended over transform.position
	
	replace it with this line:

	agent.Warp(GetSpawnPointByLevel());
	
5. now open your NetworkManagerMMO.cs and locate this line:

	prefab.transform.position = GetStartPosition().position;
	
	replace it with this line:
	
	prefab.transform.position = prefab.GetSpawnPointByLevel(0, 0);

That's it!

------------------------------------------------------------------------------------------