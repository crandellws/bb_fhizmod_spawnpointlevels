
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Networking;
 
// =======================================================================================
// SPAWN POINT
//
// 
// =======================================================================================

public class SpawnPoint : NetworkStartPosition {

	[Header("Settings")]
	[SerializeField] public int minLevel = 1;
	[SerializeField] public int maxLevel = 1;

}